module gitlab.com/ddb_db/dow

go 1.21.5

require github.com/go-resty/resty/v2 v2.11.0

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/samber/lo v1.39.0
	github.com/stretchr/testify v1.8.4
	github.com/vargspjut/wlog v1.0.11
	golang.org/x/exp v0.0.0-20240213143201-ec583247a57a
	golang.org/x/net v0.17.0 // indirect
)
