# DOW
DOW provides a cli tool for Sonarr users that allows unmonitoring of episodes by day of week.

**NOTE:** Users have reported issues with using this tool on Sonarr v3.x.  When used with older versions of Sonarr this tool will untrack random episodes that are not tagged.  This tool has only been tested with Sonarr v4.0.1.929.  It should work with any 4.x version.  The latest version will refuse to run api commands against older version of Sonarr.

## Download
Download the latest release of this tool from the project's [releases](https://gitlab.com/ddb_db/sonarr-dow/-/releases) page.

## Use Case
Many people, myself included, are only interested in The Daily Show during Jon Stewart's return run.  He's only hosting on Mondays
therefore I only want to fetch Monday shows.

## Solution
Add the following tags to The Daily Show series in Sonarr:

`dow`
`dow-1`

Then run this cli tool a few times a day and it will unmonitor any episodes that aren't on Mondays (day "1" of the week).  As you
can see, this is a generic tool that works based on series tags.  The `dow` tag is how the tool finds series you want to modify
then the `dow-<n>` tag(s) tell the tool which day(s) of the week you want to keep monitored.  1=Monday, 2=Tuesday and so on.

Run it a few times a day to make sure it sees the latest episodes in your calendar and you're good to go!

## Run as Cronjob
The way this tool works is that it fetches your calendar from Sonarr then scans it for episodes that need to be flipped.  Starting
with v0.0.4, the tool fetches the calendar for the next week and processes it on each run.  Therefore, you need to run this tool
as a cronjob (or scheduled task on Windows) periodically.  I'm running it once overnight, once around lunch time, once in the
early evening and once in the late evening.  This seems to find all episodes as needed and flips them to unmonitored.  The frequency
is up to you, but you will definitely need to run this _at least_ once a day for it to be effective.  Multiple times a day would be 
best in order to give the tool a chance to catch any metadata changes during the course of a day.

## Usage
`dow -url http://192.168.0.2:8989 -key <sonarr-api-key>`

### Helpful Options
`-version` Report the version and exit immediately.

`-loglevel <debug|info|warn|error>` Set log level; set to debug to get all of the details as to what the tool is doing when it isn't doing what you're expecting.

`-help` Display all available command options and exit immediately.
