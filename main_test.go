package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindDOWReturnsExpectedValue(t *testing.T) {
	tests := []struct {
		input    string
		expected int
		desc     string
	}{
		{"2024-02-18", 7, "Sundays must return 7 for DOW"},
		{"2024-02-17", 6, "Sat"},
		{"2024-02-16", 5, "Fri"},
		{"2024-02-15", 4, "Thu"},
		{"2024-02-14", 3, "Wed"},
		{"2024-02-13", 2, "Tue"},
		{"2024-02-12", 1, "Mon"},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			result := findDOW(tc.input)
			assert.Equal(t, tc.expected, result)
		})
	}
}

func TestBuildTagMapReturnsExpectedValue(t *testing.T) {
	// This test is all about mixing up the Sonarr tag ids and the dow day of week tag numbers
	// The final result of the map should sort things out properly, if not something is wrong
	// The final map should include the "dow" tag at idx=0 and the rest in the dow number specified
	// Notice the dow-0 tag should simply be ignored as 0 is not a valid day of the week (in this tool)
	res := []tagResource{
		{1, "dow"}, // 0
		{4, "dow-1"},
		{7, "dow-4"},
		{2, "dow-3"},
		{3, "dow-6"},
		{5, "dow-2"},
		{6, "dow-5"},
		{12, "dow-7"},
		{10, "dow-0"}, // ignored
	}

	result := buildTagMap(res)
	assert.Equal(t, []int{1, 4, 5, 2, 7, 6, 3, 12}, result)
}
