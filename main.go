package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"regexp"
	"runtime/debug"
	"strconv"
	"strings"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
	"golang.org/x/exp/slices"
)

var Version = "[dev build]"

var url *string = new(string)
var apiKey *string = new(string)

func main() {
	parseArgs()
	clnt := buildClient()
	if err := clnt.Run(); err != nil {
		panic(err)
	}
}

func buildClient() sonarrClient {
	api := resty.New()
	api.SetBaseURL(fmt.Sprintf("%s/api/v3/", *url))
	api.SetHeader("X-Api-Key", *apiKey)
	if wlog.DefaultLogger().GetLogLevel() == wlog.Dbg {
		api.SetDebug(true)
	}
	return sonarrImpl{
		api: api,
	}
}

func findBuildSetting(name string, info []debug.BuildSetting) (string, bool) {
	for _, s := range info {
		if s.Key == name {
			return s.Value, true
		}
	}
	return "", false
}

func parseArgs() {
	flag.StringVar(apiKey, "key", "", "Sonarr api key")
	flag.StringVar(url, "url", "http://localhost:8989", "Sonarr base url")
	lvl := flag.String("loglevel", "info", "log level [debug|info|warn|error]")
	doVer := flag.Bool("version", false, "show version info and exit")
	help := flag.Bool("help", false, "show usage info and exit")
	flag.Parse()

	if *doVer {
		sb := strings.Builder{}
		sb.WriteString("dow " + Version)
		if dbgData, ok := debug.ReadBuildInfo(); ok {
			sb.WriteString(" " + dbgData.GoVersion)
			if gitTag, ok := findBuildSetting("vcs.revision", dbgData.Settings); ok {
				sb.WriteString(fmt.Sprintf(" build %s", gitTag))
			}
			if mod, ok := findBuildSetting("vcs.modified", dbgData.Settings); ok {
				if mod == "true" {
					sb.WriteString("*")
				}
			}
		}
		sb.WriteString("\nhttps://gitlab.com/ddb_db/sonarr-dow")
		fmt.Println(sb.String())
		os.Exit(1)
	}

	if *help {
		flag.Usage()
		os.Exit(1)
	}

	log := wlog.DefaultLogger()
	switch strings.ToLower(*lvl) {
	case "error":
		log.SetLogLevel(wlog.Err)
	case "warn":
		log.SetLogLevel(wlog.Wrn)
	case "debug":
		log.SetLogLevel(wlog.Dbg)
	default:
		log.SetLogLevel(wlog.Nfo)
	}

	if *apiKey == "" {
		flag.Usage()
		os.Exit(1)
	}
}

type sonarrClient interface {
	Run() error
}

type series struct {
	Tags []int
}

type calendarEntry struct {
	ID        int
	AirDate   string
	Series    series
	Monitored bool
}

type sonarrVersion struct {
	Version string
}

type tagResource struct {
	ID    int
	Label string
}

type sonarrImpl struct {
	api *resty.Client
}

func (impl sonarrImpl) verifySonarrVersion() error {
	ver := &sonarrVersion{}
	req := impl.api.R()
	req.SetResult(ver)
	resp, err := req.Get("/system/status")
	if err != nil {
		return err
	}
	if resp.StatusCode() != http.StatusOK {
		return fmt.Errorf("dow: unexpected status code for status command [%d]", resp.StatusCode())
	}
	ver = resp.Result().(*sonarrVersion)
	fields := strings.Split(ver.Version, ".")
	if lo.Must(strconv.Atoi(fields[0])) < 4 {
		return fmt.Errorf("dow: this tool will only work with Sonarr v4.x or newer")
	}
	return nil
}

func (impl sonarrImpl) searchCalendar() ([]calendarEntry, error) {
	episodes := make([]calendarEntry, 0)
	req := impl.api.R()
	req.SetResult(&episodes)
	req.SetQueryParam("tags", "dow")
	req.SetQueryParam("includeSeries", "true")
	now := time.Now()
	req.SetQueryParam("start", now.Format(time.RFC3339))
	req.SetQueryParam("end", now.Add(time.Hour*24*7).Format(time.RFC3339))
	resp, err := req.Get("/calendar")
	if err != nil {
		return nil, err
	}
	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("dow: unexpected status code for calendar command [%d]", resp.StatusCode())
	}
	return *resp.Result().(*[]calendarEntry), nil
}

func (impl sonarrImpl) Run() error {
	if err := impl.verifySonarrVersion(); err != nil {
		return err
	}

	cal, err := impl.searchCalendar()
	if err != nil {
		return err
	}

	tags, err := impl.tagIDs()
	if err != nil {
		return err
	}

	ids := make([]int, 0, len(cal))
	for _, c := range cal {
		dow := findDOW(c.AirDate)
		if c.Monitored && !slices.Contains(c.Series.Tags, tags[dow]) {
			ids = append(ids, c.ID)
		}
	}
	if err := impl.unmonitorEpisodes(ids); err != nil {
		return err
	}

	return nil
}

func (impl sonarrImpl) unmonitorEpisodes(ids []int) error {
	if len(ids) > 0 {
		req := impl.api.R()
		req.SetBody(struct {
			IDs       []int `json:"episodeIds"`
			Monitored bool  `json:"monitored"`
		}{
			IDs:       ids,
			Monitored: false,
		})
		resp, err := req.Put("/episode/monitor")
		if err != nil {
			return err
		}
		if resp.StatusCode() != http.StatusAccepted {
			return fmt.Errorf("dow: unexpected status code for unmonitor command [%d]", resp.StatusCode())
		}
	}
	return nil
}

func findDOW(airDate string) int {
	t, err := time.Parse("2006-01-02", airDate)
	if err != nil {
		panic(err)
	}
	dow := t.Weekday()
	var result int
	switch dow {
	case time.Sunday:
		result = 7
	default:
		result = int(dow)
	}
	wlog.Debugf("dow for %s is %d", airDate, result)
	return result
}

func buildTagMap(tags []tagResource) []int {
	re := regexp.MustCompile(`^dow-([1-7])$`)
	ids := make([]int, 8)
	for _, t := range tags {
		if t.Label == "dow" {
			ids[0] = t.ID
			continue
		}
		matches := re.FindStringSubmatch(t.Label)
		if matches != nil {
			idx, err := strconv.Atoi(matches[1])
			if err != nil {
				panic(err)
			}
			ids[idx] = t.ID
		}
	}
	wlog.Debugf("tag map: %+v", ids)
	return ids
}

func (impl sonarrImpl) tagIDs() ([]int, error) {
	tags := make([]tagResource, 0)
	req := impl.api.R()
	req.SetResult(&tags)
	resp, err := req.Get("/tag")
	if err != nil {
		return nil, err
	}
	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("dow: unexpected status code for tag command [%d]", resp.StatusCode())
	}
	result := resp.Result().(*[]tagResource)
	return buildTagMap(*result), nil
}
